package main

import (
	"bytes"
	"html/template"
	"log"

	"gopkg.in/mail.v2"
)

func main() {
	body, err := GetEmailBody("signin")
	if err != nil {
		log.Fatal(err)
	}
	err = SendEmail(body, "zerubabelkassahun116@gmail.com", "Subscription")
	log.Println(err)
}

func SendEmail(body string, recipient string, subject string) error {

	// Set up the email message
	email := mail.NewMessage()
	email.SetHeader("From", "zershyigz@gmail.com")
	email.SetHeader("To", recipient)
	email.SetHeader("Subject", subject)
	email.SetBody("text/html", body)

	// Set up the SMTP connection details
	smtpHost := "sandbox.smtp.mailtrap.io"
	smtpPort := 2525
	smtpUsername := "f53d60c965d943"
	smtpPassword := "0c265257f043fa"

	dialer := mail.NewDialer(smtpHost, smtpPort, smtpUsername, smtpPassword)
	dialer.StartTLSPolicy = mail.MandatoryStartTLS

	if err := dialer.DialAndSend(email); err != nil {
		return err
	}

	return nil
}

// A function that return a valid html body or an error
func GetEmailBody(token string) (string, error) {

	// A template for parsing the html template and returning a valid html body
	emailBodyTemplate := template.New("email-body-template.html")
	emailBodyTemplate, err := emailBodyTemplate.ParseFiles("email-body-template.html")
	if err != nil {
		return "", err
	}

	// dummy data to test the email template
	data := EmailData{
		NumberOfVacantJobs: 21,
		VacantField:        "Software Engineering",
		VacantPositions: []VacantPosition{
			{
				PositionTitle:         "Front-End Developer",
				CompanyLogoLink:       "https://dashenbanksc.com/wp-content/uploads/Dashen-Bank-Logo-Addis-Ababa-Ethiopia.png",
				Description:           "lorem ipsum dolor sit amet consectetur adipisicing elit. Quam repudiandae suscipit, facere sint fugit",
				NumberOfLeftPositions: 125,
				CompanyName:           "Dashen Bank",
				Address:               "Addis Ababa",
				Availability:          "Full time",
				WantedPersonnel:       "1 - 2",
				PositionLink:          "/",
			},
		},
		AllJobsLink: "/",
	}

	var emailBodyBuffer bytes.Buffer

	if err := emailBodyTemplate.Execute(&emailBodyBuffer, data); err != nil {
		return "", err
	}

	return emailBodyBuffer.String(), nil
}

type VacantPosition struct {
	PositionTitle         string
	NumberOfLeftPositions int
	CompanyName           string
	CompanyLogoLink       string
	Address               string
	Availability          string
	WantedPersonnel       string
	Description           string
	PositionLink          string
}

// A struct that holds the data that will be passed to the email template
type EmailData struct {
	NumberOfVacantJobs int
	VacantField        string
	VacantPositions    []VacantPosition
	AllJobsLink        string
}
